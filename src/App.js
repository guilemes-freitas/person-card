import './App.css';
import React from 'react';
import Card from './components/Card';
import Photo from './components/Photo';



class App extends React.Component {
  person = {
    name: "Agostinho Carrara",
    age: "52",
    occupation: "Taxista",
    photo: "agostinho.jpg"
 }
  render (){
    return (
      <div className="App">
        <Photo photo={this.person.photo}/>
        <Card name={this.person.name} age={this.person.age} occupation={this.person.occupation}/>
      </div>
    );
  }
}

export default App;
