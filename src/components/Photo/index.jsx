import { Component } from 'react';
import './style.css'

class Photo extends Component{
    img_src = `./assets/img/${this.props.photo}`
    render(){
        return(
            <div className="photoBorder">
                <img src={this.img_src} alt="" className="photo"></img>
            </div>
        )
    }
}

export default Photo