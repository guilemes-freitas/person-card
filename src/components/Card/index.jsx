import { Component } from 'react';
import './style.css'

class Card extends Component{
    render(){
        return(
            <div className="card">
                <span className="cardElement">{this.props.name}</span>
                <span className="cardElement">{this.props.age}</span>
                <span className="cardElement">{this.props.occupation}</span>
            </div>
        )
    }
}

export default Card